# Dialer&Chat Ansible Playbook  
Playbook that installs and launches Dialer&Chat App. 
It will clone [frontend](https://bitbucket.org/lucvane/wsk_project_2/src/master/) and [backend](https://bitbucket.org/lucvane/wsk_project1_edited/src/master/) apps, install npm and pm2,

install all dependencies from package.json files and after that, it will start backend server and frontend app.  

[Backend app](https://bitbucket.org/lucvane/wsk_project1_edited/src/master/) won't have Dialer configured, so make sure to configure it manually in app.js file.

### Prerequisites  
* Any Linux based system  
* [Ansible](https://www.ansible.com/) 
 
## Start playbook (Ubuntu example)  
To run this playbook, you'll have to install Ansible.  
To install it, open terminal and run commands:  
```sh
sudo su  
apt install ansible
```
Open terminal and go to your playbook folder using "cd" command. 

After that, run playbook file by using command:  
```sh
ansible-playbook playbook.yml -i inventory --connection=local -K
```

